﻿using CustomIdentityJWTTokenAuthetification.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace CustomIdentityJWTTokenAuthetification.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RolesController : BaseApiController
    {
        public IHttpActionResult Get()
        {
            var roles = this.AppRoleManager.Roles;

            return Ok(roles);
        }

        public async Task<IHttpActionResult> Get(int Id)
        {
            var role = await this.AppRoleManager.FindByIdAsync(Id);

            if (role != null)
            {
                return Ok(TheModelFactory.Create(role));
            }

            return NotFound();
        }

        
        public async Task<IHttpActionResult> Post([FromBody]CreateRoleBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var role = new IdentityRole { Name = model.Name };

            var result = await this.AppRoleManager.CreateAsync(role);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            Uri locationHeader = new Uri(Url.Link("DefaultApi", new { id = role.Id }));

            return Created(locationHeader, TheModelFactory.Create(role));

        }

        public async Task<IHttpActionResult> Delete(int Id)
        {

            var role = await this.AppRoleManager.FindByIdAsync(Id);

            if (role != null)
            {
                IdentityResult result = await this.AppRoleManager.DeleteAsync(role);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return Ok();
            }

            return NotFound();

        }

        [HttpPut]
        [Route("api/roles/manageUsers", Name = "ManageUsersInRoleRoute")]
        public async Task<IHttpActionResult> ManageUsersInRole([FromBody]UsersInRoleModel model)
        {
            var role = await this.AppRoleManager.FindByIdAsync(model.Id);

            if (role == null)
            {
                ModelState.AddModelError("", "Role does not exist");
                return BadRequest(ModelState);
            }

            foreach (int userId in model.EnrolledUsers)
            {
                var appUser = await this.AppUserManager.FindByIdAsync(userId);

                if (appUser == null)
                {
                    ModelState.AddModelError("", String.Format("User: {0} does not exists", userId));
                    continue;
                }

                if (!this.AppUserManager.IsInRole(userId, role.Name))
                {
                    IdentityResult result = await this.AppUserManager.AddToRoleAsync(userId, role.Name);

                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", String.Format("User: {0} could not be added to role", userId));
                    }

                }
            }

            foreach (int userId in model.RemovedUsers)
            {
                var appUser = await this.AppUserManager.FindByIdAsync(userId);

                if (appUser == null)
                {
                    ModelState.AddModelError("", String.Format("User: {0} does not exists", userId));
                    continue;
                }

                IdentityResult result = await this.AppUserManager.RemoveFromRoleAsync(userId, role.Name);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", String.Format("User: {0} could not be removed from role", userId));
                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }
    }
}