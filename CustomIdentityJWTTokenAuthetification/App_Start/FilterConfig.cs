﻿using System.Web.Mvc;

namespace CustomIdentityJWTTokenAuthetification
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}