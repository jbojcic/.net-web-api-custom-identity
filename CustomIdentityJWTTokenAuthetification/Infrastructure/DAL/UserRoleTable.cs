﻿using CustomIdentityJWTTokenAuthetification.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.DAL
{
    /// <summary>
    /// Class that represents the UserRoles table in the database
    /// </summary>
    public class UserRoleTable
    {
        private DatabaseContext _database;

        /// <summary>
        /// Constructor that takes a DatabaseContext instance 
        /// </summary>
        /// <param name="database"></param>
        public UserRoleTable(DatabaseContext database)
        {
            _database = database;
        }

        /// <summary>
        /// Returns a list of user's roles
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<string> FindByUserId(int userId)
        {
            User user = _database.Users.Find(userId);

            return user.UserRoles.Select(i => i.Role.Name).ToList();
        }

        /// <summary>
        /// Deletes all roles from a user in the UserRoles table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public void Delete(int userId)
        {
            User user = _database.Users.Find(userId);

            foreach (UserRole record in user.UserRoles.ToList())
                _database.UserRoles.Remove(record);

            _database.SaveChanges();
        }

        /// <summary>
        /// Deletes role from a user in the UserRoles table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public void Delete(int userId, int roleId)
        {
            UserRole record = _database.UserRoles.FirstOrDefault(i => i.UserId == userId && i.RoleId == roleId);

            if (record == null)
                return;

            _database.UserRoles.Remove(record);
            _database.SaveChanges();
        }

        /// <summary>
        /// Inserts a new role for a user in the UserRoles table
        /// </summary>
        /// <param name="user">The User</param>
        /// <param name="roleId">The Role's id</param>
        /// <returns></returns>
        public void Insert(IdentityUser user, int roleId)
        {
            UserRole record = new UserRole { UserId = user.Id, RoleId = roleId };
            _database.UserRoles.Add(record);

            _database.SaveChanges();
        }
    }
}