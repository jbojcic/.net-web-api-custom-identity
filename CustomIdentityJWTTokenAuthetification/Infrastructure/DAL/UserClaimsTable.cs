﻿using CustomIdentityJWTTokenAuthetification.Models;
using System;
using System.Security.Claims;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.DAL
{
    /// <summary>
    /// Class that represents the UserClaims table in the database
    /// </summary>
    public class UserClaimsTable
    {
        private DatabaseContext _database;

        /// <summary>
        /// Constructor that takes a DatabaseContext instance 
        /// </summary>
        /// <param name="database"></param>
        public UserClaimsTable(DatabaseContext database)
        {
            _database = database;
        }

        /// <summary>
        /// Returns a ClaimsIdentity instance given a userId
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public ClaimsIdentity FindByUserId(string userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes all claims from a user given a userId
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public int Delete(string userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inserts a new claim in UserClaims table
        /// </summary>
        /// <param name="userClaim">User's claim to be added</param>
        /// <param name="userId">User's id</param>
        /// <returns></returns>
        public int Insert(Claim userClaim, string userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes a claim from a user 
        /// </summary>
        /// <param name="user">The user to have a claim deleted</param>
        /// <param name="claim">A claim to be deleted from user</param>
        /// <returns></returns>
        public int Delete(IdentityUser user, Claim claim)
        {
            throw new NotImplementedException();
        }
    }
}