﻿using CustomIdentityJWTTokenAuthetification.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.DAL
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("MyConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}