﻿using CustomIdentityJWTTokenAuthetification.Models;
using System;
using System.Linq;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.DAL
{
    /// <summary>
    /// Class that represents the Users table in the database
    /// </summary>
    public class UserTable<TUser>
        where TUser : IdentityUser
    {
        private DatabaseContext _database;

        /// <summary>
        /// Constructor that takes a DatabaseContext instance 
        /// </summary>
        /// <param name="database"></param>
        public UserTable(DatabaseContext database)
        {
            _database = database;
        }


        /// <summary>
        /// Gets all users from the Users table
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public IQueryable<IdentityUser> GetUsers()
        {
            return _database.Users.Select(i => new IdentityUser {
                Id = i.Id,
                Email = i.Email,
                EmailConfirmed = i.EmailConfirmed,
                FirstName = i.FirstName,
                LastName = i.LastName,
                UserName = i.UserName,
                PasswordHash = i.PasswordHash

            }).AsQueryable();
        }

        /// <summary>
        /// Returns the user's name given a user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserName(int userId)
        {
            User user = _database.Users.Find(userId);

            if (user == null)
                return null;
            else
                return user.UserName;
        }

        /// <summary>
        /// Returns a User ID given a user name
        /// </summary>
        /// <param name="userName">The user's name</param>
        /// <returns></returns>
        public int? GetUserId(string userName)
        {
            User user = _database.Users.FirstOrDefault(i => i.UserName == userName);

            if (user == null)
                return null;
            else
                return user.Id;
        }

        /// <summary>
        /// Returns an TUser given the user's id
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public TUser GetUserById(int userId)
        {
            User dbUser = _database.Users.Find(userId);
            return GetUser(dbUser);
        }

        /// <summary>
        /// Returns an TUser given the user's name
        /// </summary>
        /// <param name="userName">User's name</param>
        /// <returns></returns>
        public TUser GetUserByName(string userName)
        {
            User dbUser = _database.Users.FirstOrDefault(i => i.UserName == userName);
            return GetUser(dbUser);
        }

        /// <summary>
        /// Returns an TUser given the user's email
        /// </summary>
        /// <param name="email">User's email</param>
        /// <returns></returns>
        public TUser GetUserByEmail(string email)
        {
            User dbUser = _database.Users.FirstOrDefault(i => i.Email == email);
            return GetUser(dbUser);
        }

        /// <summary>
        /// Return the user's password hash
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public string GetPasswordHash(int userId)
        {
            string passHash = null;

            User dbUser = _database.Users.Find(userId);
            if (dbUser != null)
                passHash = dbUser.PasswordHash;

            if (string.IsNullOrEmpty(passHash))
            {
                return null;
            }

            return passHash;
        }

        /// <summary>
        /// Sets the user's password hash
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public void SetPasswordHash(string userId, string passwordHash)
        {
            User dbUser = _database.Users.Find(userId);
            if (dbUser != null)
            {
                dbUser.PasswordHash = passwordHash;
                _database.SaveChanges();
            }
        }

        /// <summary>
        /// Returns the user's security stamp
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetSecurityStamp(string userId)
        {
            throw new NotImplementedException();

            /*

            string stamp = null;

            User dbUser = _database.Users.Find(userId);
            if (dbUser != null)
                stamp = dbUser.SecurityStamp;

            if (string.IsNullOrEmpty(stamp))
            {
                return null;
            }

            return stamp;

            */
        }

        /// <summary>
        /// Inserts a new user in the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public void Insert(TUser user)
        {
            User dbUser = new User {

                UserName = user.UserName,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PasswordHash = user.PasswordHash,
                EmailConfirmed = user.EmailConfirmed
                /*
                SecurityStamp = user.;
                PhoneNumber = user.PhoneNumber;
                PhoneNumberConfirmed = user.PhoneNumberConfirmed;
                TwoFactorEnabled = user.TwoFactorEnabled;
                DateTime? LockoutEndDateUtc = user.LockoutEndDateUtc;
                bool LockoutEnabled = user.LockoutEnabled;
                int AccessFailedCount = user.AccessFailedCount;
                */
            };

            _database.Users.Add(dbUser);
            _database.SaveChanges();
            user.Id = dbUser.Id;
        }

        /// <summary>
        /// Deletes a user from the Users table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        private void Delete(int userId)
        {
            User dbUser = _database.Users.Find(userId);
            if (dbUser != null)
            {
                _database.Users.Remove(dbUser);
                _database.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes a user from the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public void Delete(TUser user)
        {
            Delete(user.Id);
        }

        /// <summary>
        /// Updates a user in the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public void Update(TUser user)
        {
            User dbUser = _database.Users.Find(user.Id);

            if (dbUser == null)
                return;

            dbUser.Id = user.Id;
            dbUser.UserName = user.UserName;
            dbUser.Email = user.Email;
            dbUser.EmailConfirmed = user.EmailConfirmed;
            dbUser.PasswordHash = user.PasswordHash;

            /*

            dbUser.SecurityStamp = user.SecurityStamp;
            dbUser.PhoneNumber = user.PhoneNumber;
            dbUser.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
            dbUser.LockoutEnabled = user.LockoutEnabled;
            dbUser.LockoutEndDateUtc = user.LockoutEndDateUtc;
            dbUser.AccessFailedCount = user.AccessFailedCount;

            */

            _database.SaveChanges();
        }


        #region Helper Methods

        private TUser GetUser(User dbUser)
        {
            if (dbUser == null)
                return null;

            TUser user = (TUser)Activator.CreateInstance(typeof(TUser));
            user.Id = dbUser.Id;
            user.UserName = dbUser.UserName;
            user.Email = dbUser.Email;
            user.EmailConfirmed = dbUser.EmailConfirmed;
            user.PasswordHash = dbUser.PasswordHash;
            user.FirstName = dbUser.FirstName;
            user.LastName = dbUser.LastName;

            /*

            user.SecurityStamp = dbUser.SecurityStamp;
            user.PhoneNumber = dbUser.PhoneNumber;
            user.PhoneNumberConfirmed = dbUser.PhoneNumberConfirmed;
            user.LockoutEnabled = dbUser.LockoutEnabled;
            user.LockoutEndDateUtc = dbUser.LockoutEndDateUtc;
            user.AccessFailedCount = dbUser.AccessFailedCount;

            */

            return user;
        }

        #endregion
    }
}