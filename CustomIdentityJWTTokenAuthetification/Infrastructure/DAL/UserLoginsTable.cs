﻿using CustomIdentityJWTTokenAuthetification.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.DAL
{
    /// <summary>
    /// Class that represents the UserLogins table in the database
    /// </summary>
    public class UserLoginsTable
    {
        private DatabaseContext _database;

        /// <summary>
        /// Constructor that takes a DatabaseContext instance 
        /// </summary>
        /// <param name="database"></param>
        public UserLoginsTable(DatabaseContext database)
        {
            _database = database;
        }

        /// <summary>
        /// Deletes a login from a user in the UserLogins table
        /// </summary>
        /// <param name="user">User to have login deleted</param>
        /// <param name="login">Login to be deleted from user</param>
        /// <returns></returns>
        public int Delete(IdentityUser user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes all Logins from a user in the UserLogins table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public int Delete(string userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inserts a new login in the UserLogins table
        /// </summary>
        /// <param name="user">User to have new login added</param>
        /// <param name="login">Login to be added</param>
        /// <returns></returns>
        public int Insert(IdentityUser user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Return a userId given a user's login
        /// </summary>
        /// <param name="userLogin">The user's login info</param>
        /// <returns></returns>
        public string FindUserIdByLogin(UserLoginInfo userLogin)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a list of user's logins
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<UserLoginInfo> FindByUserId(string userId)
        {
            throw new NotImplementedException();
        }
    }
}