﻿using CustomIdentityJWTTokenAuthetification.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.DAL
{
    /// <summary>
    /// Class that represents the Role table in the database
    /// </summary>
    public class RoleTable
    {
        private DatabaseContext _database;

        /// <summary>
        /// Constructor that takes a DatabaseContext instance 
        /// </summary>
        /// <param name="database"></param>
        public RoleTable(DatabaseContext database)
        {
            _database = database;
        }

        /// <summary>
        /// Gets all roles from the Roles table
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public IQueryable<IdentityRole> GetRoles()
        {
            return _database.Roles.Select(i => new IdentityRole { Id = i.Id, Name = i.Name }).AsQueryable();
        }

        /// <summary>
        /// Deltes a role from the Roles table
        /// </summary>
        /// <param name="roleId">The role Id</param>
        /// <returns></returns>
        public void Delete(int roleId)
        {
            Role dbRole = _database.Roles.Find(roleId);
            _database.Roles.Remove(dbRole);
            _database.SaveChanges();
        }

        /// <summary>
        /// Inserts a new Role in the Roles table
        /// </summary>
        /// <param name="roleName">The role's name</param>
        /// <returns></returns>
        public void Insert(IdentityRole role)
        {
            Role dbRole = new Role { Name = role.Name };
      
            _database.Roles.Add(dbRole);
            _database.SaveChanges();
            role.Id = dbRole.Id;
        }

        /// <summary>
        /// Returns a role name given the roleId
        /// </summary>
        /// <param name="roleId">The role Id</param>
        /// <returns>Role name</returns>
        public string GetRoleName(int roleId)
        {
            Role role = _database.Roles.Find(roleId);
            if (role == null)
                return null;

            return role.Name;
        }

        /// <summary>
        /// Returns the role Id given a role name
        /// </summary>
        /// <param name="roleName">Role's name</param>
        /// <returns>Role's Id</returns>
        public int? GetRoleId(string roleName)
        {
            Role role = _database.Roles.FirstOrDefault(i => i.Name == roleName);
            if (role == null)
                return null;

            return role.Id;
        }

        /// <summary>
        /// Gets the IdentityRole given the role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public IdentityRole GetRoleById(int roleId)
        {
            var roleName = GetRoleName(roleId);
            IdentityRole role = null;

            if (roleName != null)
            {
                role = new IdentityRole(roleName, roleId);
            }

            return role;

        }

        /// <summary>
        /// Gets the IdentityRole given the role name
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public IdentityRole GetRoleByName(string roleName)
        {
            var roleId = GetRoleId(roleName);
            IdentityRole role = null;

            if (roleId != null)
            {
                role = new IdentityRole(roleName, (int)roleId);
            }

            return role;
        }

        public void Update(IdentityRole role)
        {
            Role dbRole = _database.Roles.Find(role.Id);

            if (dbRole == null)
                return;

            dbRole.Name = role.Name;
            _database.SaveChanges();
        }
    }
}