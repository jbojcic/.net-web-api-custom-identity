﻿using CustomIdentityJWTTokenAuthetification.Infrastructure.DAL;
using CustomIdentityJWTTokenAuthetification.Infrastructure.Storage;
using CustomIdentityJWTTokenAuthetification.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomIdentityJWTTokenAuthetification.Infrastucture.Managers
{
    public class ApplicationRoleManager : RoleManager<IdentityRole, int>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, int> store)
            : base(store)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var appRoleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<DatabaseContext>()));

            return appRoleManager;
        }
    }
}