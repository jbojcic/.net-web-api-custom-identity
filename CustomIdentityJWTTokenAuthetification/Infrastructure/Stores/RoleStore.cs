﻿using CustomIdentityJWTTokenAuthetification.Infrastructure.DAL;
using CustomIdentityJWTTokenAuthetification.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CustomIdentityJWTTokenAuthetification.Infrastructure.Storage
{
    /// <summary>
    /// Class that implements the key ASP.NET Identity role store iterfaces
    /// </summary>
    public class RoleStore<TRole> : IQueryableRoleStore<TRole, int>
                                    where TRole : IdentityRole
    {
        private RoleTable roleTable;
        public DatabaseContext Database { get; private set; }

        public IQueryable<TRole> Roles
        {
            get
            {
                return roleTable.GetRoles() as IQueryable<TRole>;
            }
        }


        /// <summary>
        /// Default constructor that initializes a new DatabaseContext
        /// instance using the Default Connection string
        /// </summary>
        public RoleStore()
        {
            new RoleStore<TRole>(new DatabaseContext());
        }

        /// <summary>
        /// Constructor that takes a DatabaseContext as argument 
        /// </summary>
        /// <param name="database"></param>
        public RoleStore(DatabaseContext database)
        {
            Database = database;
            roleTable = new RoleTable(database);
        }

        public Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            roleTable.Insert(role);

            return Task.FromResult<object>(null);
        }

        public Task DeleteAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("user");
            }

            roleTable.Delete(role.Id);

            return Task.FromResult<Object>(null);
        }

        public Task<TRole> FindByIdAsync(int roleId)
        {
            TRole result = roleTable.GetRoleById(roleId) as TRole;

            return Task.FromResult<TRole>(result);
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            TRole result = roleTable.GetRoleByName(roleName) as TRole;

            return Task.FromResult<TRole>(result);
        }

        public Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("user");
            }

            roleTable.Update(role);

            return Task.FromResult<Object>(null);
        }

        public void Dispose()
        {
            if (Database != null)
            {
                Database.Dispose();
                Database = null;
            }
        }

    }
}