﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CustomIdentityJWTTokenAuthetification.Models
{
    public class IdentityUser : IUser<int>
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual string Email { get; set; }

        public virtual bool EmailConfirmed { get; set; }

        public virtual string PasswordHash { get; set; }

        public virtual string SecurityStamp { get; set; }

        public virtual string PhoneNumber { get; set; }

        public virtual bool PhoneNumberConfirmed { get; set; }

        public virtual bool TwoFactorEnabled { get; set; }

        public virtual DateTime? LockoutEndDateUtc { get; set; }

        public virtual bool LockoutEnabled { get; set; }

        public virtual int AccessFailedCount { get; set; }

        //public virtual ICollection<IdentityRole> Roles { get; private set; }

        public IdentityUser() { }

        public IdentityUser(string userName)
            : this()
        {
            UserName = userName;
        }
    }

    public class IdentityRole : IRole<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }


        public IdentityRole() { }

        public IdentityRole(string name) : this()
        {
            Name = name;
        }

        public IdentityRole(string name, int id)
        {
            Name = name;
            Id = id;
        }
    }

    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here

            userIdentity.AddClaim(new Claim("firstname", FirstName));
            userIdentity.AddClaim(new Claim("lastname", LastName));

            return userIdentity;
        }
    }
}