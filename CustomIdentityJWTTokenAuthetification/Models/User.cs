﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomIdentityJWTTokenAuthetification.Models
{
    public partial class User
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PasswordHash { get; set; }

        public bool EmailConfirmed { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }

        public User()
        {
            this.UserRoles = new HashSet<UserRole>();
        }
    }
}