namespace CustomIdentityJWTTokenAuthetification.Migrations
{
    using Infrastucture.Managers;
    using Infrastructure.DAL;
    using Infrastructure.Storage;
    using Microsoft.AspNet.Identity;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CustomIdentityJWTTokenAuthetification.Infrastructure.DAL.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CustomIdentityJWTTokenAuthetification.Infrastructure.DAL.DatabaseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var manager = new UserManager<ApplicationUser, int>(new UserStore<ApplicationUser>(new DatabaseContext()));

            var roleManager = new RoleManager<IdentityRole, int>(new RoleStore<IdentityRole>(new DatabaseContext()));

            var user = new ApplicationUser()
            {
                UserName = "jbojcic",
                Email = "jbojcic@fesb.hr",
                EmailConfirmed = true,
                FirstName = "Josip",
                LastName = "Bojcic"
            };

            manager.Create(user, "MySuperP@ss!");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
            }

            var adminUser = manager.FindByName("jbojcic");

            manager.AddToRoles(adminUser.Id, new string[] { "Admin" });
        }
    }
}
